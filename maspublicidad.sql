-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-02-2018 a las 14:03:32
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `maspublicidad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `contenido` text NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `titulo`, `imagen`, `contenido`, `created_at`) VALUES
(1, 'IMPORTANCIA DE LAS REDES SOCIALES', 'redsocial.png', '<p></p><p>En un principio, la Comunicación Entre Ordenadores era bastante simple, siendo una experimentación que buscaba unir distintos equipos pertenecientes a Redes Estudiantiles para realizar un básico Intercambio de Información destinado a fines educativos, teniendo posterior crecimiento en las redes primitivas que posteriormente fueron el puntapié inicial de lo que hoy en día es nuestra amada red de redes. <br><br>Fue así que comenzaron a aparecer los primeros Sitios de Internet que requerían de un servidor específico, conocimientos bastante avanzados para la época y mucha dificultad a la hora de montar una Página Web Propia, pero esto fue evolucionando progresivamente hasta llegar al concepto de Web 2.0, donde los usuarios son los protagonistas compartiendo lo máximo que puedan. <br><br>El mundo moderno nos ve prácticamente Conectados a Internet en todo momento y en todo lugar, contando no solo con la posibilidad de utilizar un Ordenador Portátil, sino también utilizar el servicio de Internet Móvil, siendo esta herramienta la que nos permite realizar lo anteriormente mencionado, subiendo constantemente información a la nube, sobre todo con el auge de las Redes Sociales. <br><br>Este tipo de sitio web no es más que una enorme sala de Intercambio de Información donde los usuarios comparten con otros todas las actividades que realizan, así como Contenidos Multimedia o bien simplemente textos, estando en contacto constante y con Actualizaciones en Tiempo Real, realizando intercambios de información y dando lugar a debates y comentarios sobre un contenido en particular. <br><br>Las redes sociales más populares hoy en día son Facebook, que contiene una actualización en tiempo real basada en compartir Actualizaciones de Estado, Contenidos Multimedia y Enlaces, y por otro lado tenemos a Twitter, donde todos los usuarios están inmersos en una especie de Sala de Chat universal, compartiendo mensajes de 140 caracteres y pudiendo realizarse menciones y mensajes privados entre cada uno de los miembros registrados.</p><p><a target="_blank" rel="nofollow" href="https://www.importancia.org/redes-sociales.php">https://www.importancia.org/redes-sociales.php</a>&nbsp;(Creditos)</p><br><p></p>', '2018-02-13 20:39:30'),
(2, '¿CUÁLES SON LAS VENTAJAS DE UNA PÁGINA ADAPTADA PARA MÓVILES?', 'responsive.jpg', '<p>Es la creación de un único sitio web que pueda ser visualizado perfectamente en todo tipo de dispositivos, desde ordenadores de escritorio hasta smartphones o tablets. <br><br><b>Ventajas de utilizar responsive design:</b><br><br>1. El uso de móviles está aumentando: El pasado año más del 86% de la población accede a Internet desde Smartphones y más de un 45% desde tablets según un estudio del IAB Spain en 2013. <br>Los usuarios se conectan una media de 2 horas y media al día a través de los smartphone <br><br>2. Recomendado por Google, una web creada con responsive web design mejora el SEO, las búsquedas a través de dispositivos móviles son diferentes a las que se realizan desde un ordenador de escritorio por lo que tener una versión móvil de tu web es bueno. Tener un site con responsive design es aún mejor, sobre todo porque cuantos más dispositivos alcances a cubrir más tráfico generará tu web. <br><br>3. Puede aumentar las tasas de conversión. Según un informe de Google, el 67% de los usuarios móviles son más propensos a comprar un producto o servicio desde una web móvil amigable. <br><br>4. Mejorar la experiencia del usuario. Los internautas no tienen que perder el tiempo con el zoom y encoger el texto o las imágenes en la pantalla. En lugar de ello, todo el contenido se ajusta automáticamente a la pantalla del dispositivo. <br><br>5. Más fácil de administrar. Si tenemos sitios independientes para móviles y escritorio, las campañas SEO serán diferentes para cada uno. ¿No sería más fácil tener solo una campaña SEO y un sitio web? Esto es más rentable. <br><br>6. Mantente a la vanguardia de la competencia. Si cada vez más usuarios están utilizando sus dispositivos móviles para buscar y realizar compras en línea. Para ello, debes tener una web responsive para atender a ese público potencial.<br></p>', '2018-02-13 20:59:10'),
(3, '5 RAZONES PARA TENER UNA PÁGINA WEB PARA TU NEGOCIO EN PANAMÁ ACTUALMENTE', 'noticia3.jpg', '<p>El problema es que un 35% de las micro-empresas Panameñas no están conectadas a una pagina web, según algunos estudios. <br><br>En el caso de los autónomos, el porcentaje es peor. Los motivos que se esconden tras este hecho suelen ser siempre los mismos: el presupuesto y el tiempo, o ambos. Pero en su gran mayoría son solo excusas para " ahorrar " un dinero que claramente retornara si se hace bien, siguen la chistosa filosofía de " Quitarle las pilas a su reloj para que el tiempo no pase " , eventualmente su competencia evolucionara a un website y será un tanto tarde para " salir " al mercado virtual. <br><br><b>1) Correcta presentación de su oferta o producto:</b><br><br>Planificar correctamente una presencia en Internet incluye el ocuparse de manera intensa de la oferta de productos o servicios. Esto, de vez en cuando, es un aspecto más importante de lo que los clientes creen: de esta manera, la clientela ocasional de una tienda de zapatos, por ejemplo, puede enterarse en la página de inicio de que a parte de haber calzado para niños, también se vende calzado de señora y accesorios.<br><br><b>2) Publicidad efectiva:</b><br><br>¿Cómo es su publicidad? Funciona? Ya se trate de correos electrónicos, flyers o cualquier otro método. Estas técnicas presionan al cliente y raramente contactan con él en el momento adecuado. Por el contrario, si éste lo haces en Google ó redes sociales, las posibilidades de que un cliente potencial se ponga en contacto contigo son mayores. Conseguir publicidad muy barata o gratuita es prácticamente imposible: Aquellos visitantes que llegan a través de búsquedas normales (orgánicas) no representan gastos. Está claro que la creación, mantenimiento y optimización de una página web exige una primera inversión, pero véase el motivo número 3.<br><br><b>3) Publicar su marca en Internet es 500 veces mas económico que la publicidad tradicional.</b><br><br>Actualmente pautar tu marca en un medio convencional te garantiza una cantidad de miles de posibles prospecto que recibirán tu propuesta, pero al publicar en Internet tienes un mercado de millones posibles prospecto. <br>La estadística actual muestra que un medio responsable tiene un costo mensual entre $ 800 y $ 1,500 mensuales. <br>Para darles una idea, con $ 80.00 mensuales puedes obtener 12,000 visitas a tu web aproximadamente, no es lo mismo que lo vean a que puedan acceder a su producto o servicio con un solo clic. Ahora imagen que pasaría con una inversión de $ 500 - $ 1,000 multipliquen esas posibles visitas que pueden convertirse en posibles ventas? matemática básica amigos.<br><br><b>4) Tus clientes van hacia ti:</b><br><br>En caso de que recibas clientes de forma habitual en tu oficina o empresa, una descripción de cómo llegar es de lo más útil. Integrar para ello Google Maps, por ejemplo, y cuando naveguen en tu web pueden llegar directamente a tu oficina.<br><br><b>5) Respuesta a las solicitudes de clientes a través de la web: </b><br><br>Lo más seguro es que se te ocurran dos o tres preguntas que tus clientes hacen con bastante asiduidad. Una entretenida conversación telefónica con los clientes siempre es bienvenida. No obstante, seguro que también puedes invertir tu tiempo en cosas más productivas que responder las mismas preguntas una y otra vez. En un apartado de “Preguntas frecuentes” puedes contestar directamente a estas preguntas en la página web. Para todas las demás preguntas, puedes crear un formulario de contacto online (y en caso de que te guste tratar por teléfono a los clientes, incluye también el teléfono de contacto)<br><br></p>', '2018-02-13 21:00:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL,
  `link` varchar(500) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `link`, `titulo`, `imagen`, `created_at`) VALUES
(1, 'http://kassa.com.pa/', 'Kassa', 'cliente1.jpg', '2018-02-10 22:05:27'),
(2, 'http://brpedros.com/', 'BrPedros', 'cliente2.jpg', '2018-02-10 22:12:11'),
(3, 'http://drakellys.com/', 'Dra Kellys', 'cliente3.jpg', '2018-02-10 22:16:55'),
(4, 'http://www.dreamplaza.com.pa/', 'Dream Plaza', 'cliente4.jpg', '2018-02-10 22:26:54'),
(5, 'http://www.rosaslaw.com/', 'RosasLaw', 'cliente5.jpg', '2018-02-10 22:31:43'),
(6, 'http://windrosepanama.com/', 'Windrose Panama', 'cliente6.jpg', '2018-02-10 22:32:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int(11) NOT NULL,
  `whatsapp` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo` varchar(200) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `skype` varchar(200) NOT NULL,
  `ubicacion` varchar(200) NOT NULL,
  `horario` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id`, `whatsapp`, `telefono`, `correo`, `facebook`, `instagram`, `skype`, `ubicacion`, `horario`) VALUES
(1, ' 507-6251-5479', '507-395-4758', ' servicios@maspublicidad.net', '#', '#', '#', 'Panama, Ciudad de Panama', 'Horario 8:00 am - 19:00 pm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links_footer`
--

CREATE TABLE IF NOT EXISTS `links_footer` (
  `id` int(11) NOT NULL,
  `texto` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `tipo` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `links_footer`
--

INSERT INTO `links_footer` (`id`, `texto`, `link`, `tipo`) VALUES
(1, 'Landing Page', '#web', 'QUIERES UN WEB'),
(2, 'Micro Webs', '#web', 'QUIERES UN WEB'),
(3, 'Ecommerce', '#web', 'QUIERES UN WEB'),
(4, 'A la medida', '#web', 'QUIERES UN WEB'),
(5, 'Diseno Grafico', '#servicios', 'SERVICIOS'),
(6, 'Online Marketing', '#servicios', 'SERVICIOS'),
(7, 'Multimedia', '#servicios', 'SERVICIOS'),
(8, 'Redes sociales', '#servicios', 'SERVICIOS'),
(9, 'Video', '#servicios', 'SERVICIOS'),
(10, 'Que somos', '#nosotros', 'OTROS'),
(11, 'Clientes', '#clientes', 'OTROS'),
(12, 'Trabaja con nosotros', '#', 'OTROS'),
(13, 'FAQ', '#', 'OTROS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `necesitas`
--

CREATE TABLE IF NOT EXISTS `necesitas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `sub_titulo` varchar(500) NOT NULL,
  `texto_boton` varchar(200) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `necesitas`
--

INSERT INTO `necesitas` (`id`, `titulo`, `sub_titulo`, `texto_boton`, `imagen`) VALUES
(1, 'ROTULACION, LETREROS, VALLAS DE CARRETERA, VINYL', 'Cotice al mejor precio y calidad del mercado', 'Cotizar', 'vallas_rotulos.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `necesitas_app`
--

CREATE TABLE IF NOT EXISTS `necesitas_app` (
  `id` int(11) NOT NULL,
  `imagen` varchar(500) NOT NULL,
  `link` varchar(300) NOT NULL,
  `subtitulo` varchar(300) NOT NULL,
  `texto_boton` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `necesitas_app`
--

INSERT INTO `necesitas_app` (`id`, `imagen`, `link`, `subtitulo`, `texto_boton`, `created_at`) VALUES
(1, 'app.png', '#contacto', 'Llevamos tu idea al siguiente nivel.', 'Contar idea', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE IF NOT EXISTS `portafolio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  `trabajo_realizado` varchar(200) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `portafolio`
--

INSERT INTO `portafolio` (`id`, `titulo`, `link`, `trabajo_realizado`, `imagen`, `created_at`) VALUES
(1, 'Windrose Panama', 'http://windrosepanama.com/', 'Website', 'portafolio1.jpg', NULL),
(2, 'Efecto Mela', 'http://efectomela.com/', 'Websites - Landing page', 'portafolio2.jpg', '2018-02-14 02:35:44'),
(3, 'Dra Kellys', 'http://drakellys.com/', 'Websites - Landing page', 'portafolio3.jpg', '2018-02-14 02:37:25'),
(4, 'Kassa', 'http://kassa.com.pa/', 'Websites', 'portafolio4.jpg', '2018-02-14 02:44:39'),
(5, 'Grupo los Guayacanes', NULL, 'Diseno Grafico - Rediseno - Logo', 'portafolio5.jpg', '2018-02-14 02:47:41'),
(6, 'AITGP', NULL, 'Diseno Grafico - Rediseno - Logo', 'portafolio6.jpg', '2018-02-14 02:49:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quienes_somos`
--

CREATE TABLE IF NOT EXISTS `quienes_somos` (
  `id` int(11) NOT NULL,
  `contenido` text NOT NULL,
  `imagen` varchar(600) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `quienes_somos`
--

INSERT INTO `quienes_somos` (`id`, `contenido`, `imagen`) VALUES
(1, 'Somos una agencia digital boutique, con 14 años de experiencia en el mercado Panameño, que ha logrado trabajar con las marcas más importantes del país, nuestros mas de 300 casos de éxito nos avalan y hablan por nosotros. Al NO SER una Publicitaria, ofrecemos servicios digitales que ayudan a resaltar su empresa en un mercado pequeño, pero agresivo donde una pequeña diferencia puede tu punto de partida, nuestra principal herramienta es el Internet, sin omitir los servicios tradicionales de mercadeo.', 'nosotros.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quieres_web`
--

CREATE TABLE IF NOT EXISTS `quieres_web` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `titulo1` varchar(500) NOT NULL,
  `subtitulo1` varchar(500) NOT NULL,
  `titulo2` varchar(500) NOT NULL,
  `subtitulo2` varchar(500) NOT NULL,
  `titulo3` varchar(500) NOT NULL,
  `subtitulo3` varchar(500) NOT NULL,
  `titulo4` varchar(500) NOT NULL,
  `subtitulo4` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `quieres_web`
--

INSERT INTO `quieres_web` (`id`, `titulo`, `titulo1`, `subtitulo1`, `titulo2`, `subtitulo2`, `titulo3`, `subtitulo3`, `titulo4`, `subtitulo4`) VALUES
(1, '¿ Sabias que? Actualmente una pagina web es la diferencia entre conseguir un cliente o perderlo para siempre?\r\n<br>\r\nNo permitas que eso suceda, comienza ahora, escoge :', 'Landing Page', 'Ideal para promocionar servicios o productos específico', 'Micro Webs', 'Presupuesto bajo? No es ningún problema, esta será tu punta de lanza', 'Ecommerce', 'Vende desde tu web, rápido, fácil y seguro', 'A la medida', 'Sabes lo que quieres y tu web debe ser único, nos encanta!');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `resumen` varchar(500) NOT NULL,
  `imagen` varchar(300) DEFAULT NULL,
  `color` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `titulo`, `resumen`, `imagen`, `color`) VALUES
(1, 'Diseño Grafico', 'Logos, Branding, Artes', 'servicioa.jpg', '#2d96db'),
(2, 'Online Marketing', 'SEO, Email marketing, Adwords', 'servicio_online.jpg', '#8bb51a'),
(3, 'Proyectos Inmobiliarios ', 'Renders 3D, Planos 3D, Perspectivas 3D', 'render.jpg', '#e7b628'),
(4, 'Redes Sociales ', 'Twitter, Facebook, Instagram...', 'serviciod.jpg', '#1a1a1a'),
(5, 'Video', 'Whiteboards Video, 2d Video, 3d Video, Corporativos, TV', 'servicioe.jpg', '#ff5151'),
(6, 'HOSTING Y DOMINIOS ', 'Dominio y Hosting Gratis por 1 año', 'serviciog.jpg', '#53057b');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) DEFAULT NULL,
  `subtitulo` text,
  `resumen` varchar(500) DEFAULT NULL,
  `color_texto` varchar(20) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `boton1` varchar(200) DEFAULT NULL,
  `link1` varchar(200) DEFAULT NULL,
  `boton2` varchar(200) DEFAULT NULL,
  `link2` varchar(200) DEFAULT NULL,
  `boton3` varchar(200) DEFAULT NULL,
  `link3` varchar(200) DEFAULT NULL,
  `boton4` varchar(200) DEFAULT NULL,
  `link4` varchar(200) DEFAULT NULL,
  `boton5` varchar(200) DEFAULT NULL,
  `link5` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sliders`
--

INSERT INTO `sliders` (`id`, `titulo`, `subtitulo`, `resumen`, `color_texto`, `imagen`, `boton1`, `link1`, `boton2`, `link2`, `boton3`, `link3`, `boton4`, `link4`, `boton5`, `link5`) VALUES
(2, 'Bienvenido,', 'Asi que quieres resaltar en el mercado? <br>Nosotros podemos ayudarte.', 'Somos expertos, con deseos de ayudarte <br> a crecer en internet, asi como hemos ayudado <br> a docenas de marcas.', '#333333', 'banner.png', 'Ver servicios', '#servicios', 'Escribenos', '#contacto', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Que buscas?', 'Quieres publicidad que genere ventas?', NULL, '#333333', 'banner1.jpg', 'Redes sociales', '#servicios', 'Mercadeo online', '#servicios', 'Diseño grafico', '#servicios', 'SEO', '#servicios', NULL, NULL),
(4, 'Buscas una pagina web?', NULL, NULL, '#333333', 'banner3.jpg', 'Landing page', '#web', 'Microweb', '#web', 'Standard', '#web', 'A la medida', '#web', NULL, NULL),
(5, 'Nosotros somos expertos! ', 'Quieres tu marca en las redes sociales? ', NULL, '#333333', 'banner4.jpg', 'Administración de redes', '#servicios', 'Publicidad en redes', '#servicios', 'artes para redes', '#servicios', 'Vídeo para redes', '#servicios', NULL, NULL),
(6, 'que impacta', 'Hacemos publicidad', NULL, '#ffffff', 'banner5.jpg', 'Letreros', '#servicios', 'Vallas de carretera', '#servicios', 'Banners', '#servicios', 'Rotulos', '#servicios', 'Acrilico', '#servicios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

CREATE TABLE IF NOT EXISTS `testimonios` (
  `id` int(11) NOT NULL,
  `contenido` text NOT NULL,
  `trabajo_realizado` varchar(300) NOT NULL,
  `firma` varchar(200) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `testimonios`
--

INSERT INTO `testimonios` (`id`, `contenido`, `trabajo_realizado`, `firma`, `imagen`, `created_at`) VALUES
(1, '<p>Hemos trabajado desde hace unos años con Mas Publicidad, Cero quejas!.</p>', 'Pagina Web', 'Juan Manuel Suarez – Gerente Comercial', 'suarez.png', '2018-02-10 00:00:00'),
(2, '<p>Fueron muy pacientes con los cambios, nos asesoraron en todo momento, el resultado fue muy gratificante , recomendado 100%</p>', 'Pagina Web', 'Ingryd – Marketing Manager', 'dream.png', '2018-02-10 20:20:47'),
(3, '<p>Simplemente fabuloso, desde que  iniciamos nuestra relación comercial con Mas  Publicidad hace 6 años siempre han cuidado de brindar el mejor servicio, muy satisfechos.</p>', 'Pagina Web', 'Lic. Ana Gloria Roman – Gerente de Medios – Diseño Web', 'rosas.png', '2018-02-10 20:28:43'),
(4, '<p>Trabajamos muchos servicios con Mas Publicidad, todos siempre con resultados positivos, un servicio de primera y siempre dispuestos ayudar, inclusive fuera de horario , gracias.</p>', 'Pagina Web', 'Arq. Karithsy Pereira – Gerente de Marca', 'coinla.png', '2018-02-10 20:31:14'),
(5, '<p>Resideñamos nuestro web con Mas Publicidad, asi como otros servicios de rotulación , todo salio mejor de lo que pensamos, los recomiendo abiertamente.</p>', 'Pagina Web', 'Carlos Marquez – Gerente General', 'kassa.png', '2018-02-10 20:34:04'),
(6, '<p>Comprendieron nuestra falta de conocimiento en el tema, fueron muy pacientes, el resultado fue el esperado, aun seguimos cotizando servicios adicionales con ellos.</p>', 'Pagina Web', 'Sra. Rosaura – Gerente de Ventas', 'adelle.png', '2018-02-10 20:36:11'),
(7, '<p>Necesitaba un sitio web para mi marca, Mas Publicidad me ayudo y comprendio sin mucho esfuerzo, estoy super contenta con mi web.</p>', 'Pagina Web', 'Melissa Piedrahita – CEO', 'mela.png', '2018-02-10 20:38:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`) VALUES
(1, 'admin', '123', 'Administrador', 'maspublicidad');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `links_footer`
--
ALTER TABLE `links_footer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `necesitas`
--
ALTER TABLE `necesitas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `necesitas_app`
--
ALTER TABLE `necesitas_app`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `quienes_somos`
--
ALTER TABLE `quienes_somos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `quieres_web`
--
ALTER TABLE `quieres_web`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `links_footer`
--
ALTER TABLE `links_footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `necesitas`
--
ALTER TABLE `necesitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `necesitas_app`
--
ALTER TABLE `necesitas_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `quienes_somos`
--
ALTER TABLE `quienes_somos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `quieres_web`
--
ALTER TABLE `quieres_web`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
