<?php
/**
 * @see Controller nuevo controller
 */
require_once CORE_PATH . 'kumbia/controller.php';

/**
 * Controlador principal que heredan los controladores
 *
 * Todas las controladores heredan de esta clase en un nivel superior
 * por lo tanto los metodos aqui definidos estan disponibles para
 * cualquier controlador.
 *
 * @category Kumbia
 * @package Controller
 */
class AppController extends Controller
{

    final protected function initialize()
    {
      if ( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['comments']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
             if(Input::post("g-recaptcha-response")!= ''){

                 $headers = 'From: ' . $_POST["name"] . '<' . $_POST["email"] . '>' . "\r\n" .
                 'Reply-To: ' . $_POST["email"] . "\r\n" .
                 'X-Mailer: PHP/' . phpversion();

                 $message = sprintf( "Nombre: %s\nEmpresa: %s\nEmail: %s\nServicio: %s\nTelefono: %s\nCelular: %s\n\n Mensaje: %s",
                 $_POST['name'], $_POST['empresa'], $_POST['email'], $_POST['servicio'], $_POST['phone'], $_POST['celular'], $_POST['comments'] );
                 $success = mail( "gerencia_mp@paginaswebenpanama.net", "Mensaje de Contacto", $message, $headers );
                 mail( "steffanytj@gmail.com", "Mensaje de Contacto JEAN", $message, $headers );
                 //  Replace with your email gerencia_mp@paginaswebenpanama.net
                 if ($success) {
                   Flash::valid("<strong>Enviado!</strong> Mensaje enviado satisfactoriamente");
                 }else{
                   Flash::error("<strong>Error de envio, intente de nuevo</strong>");
                 }
               }else{
                 Flash::error("<strong>Debe marcar el captcha</strong>");
               }
        }
      $this->quienes_somos = (new Quienes_somos())->find_first();
      $this->testimonios = (new Testimonios())->find("order: created_at desc");
      $this->clientes = (new Clientes())->find("order: created_at desc");
      $this->necesitas_app = (new Necesitas_app())->find_first();
      $this->servicios = (new Servicios())->find();
      $this->necesitas = (new Necesitas())->find_first();
      $this->quieres_web = (new Quieres_web())->find_first();
      $this->sliders = (new Sliders())->find();
      $this->contacto = (new Contacto())->find_first();
      $this->portafolio = (new Portafolio())->find();
      $this->links_footer1 = (new Links_footer())->find(
        "conditions: tipo='QUIERES UN WEB'",
        "order: id");
      $this->links_footer2 = (new Links_footer())->find(
        "conditions: tipo='SERVICIOS'",
        "order: id");
      $this->links_footer3 = (new Links_footer())->find(
        "conditions: tipo='OTROS'",
        "order: id");
      $this->blogs = (new Blogs())->find(
        "limit: 3",
        "order: created_at desc"
      );

    }

    final protected function finalize()
    {

    }

}
