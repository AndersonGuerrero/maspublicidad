<?php
class IndexController extends AppController
{
    public function blog($id)
    {
      $this->blog = ( new Blogs())->find_by_id((int)$id);
      $this->blogsList = (new Blogs())->find(
        "limit: 6",
        "order: created_at desc",
        "conditions: not id=".$id
      );
    }

    public function index()
    {

    }
}
