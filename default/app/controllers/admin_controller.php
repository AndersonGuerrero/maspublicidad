<?php
class AdminController extends CustomAdminController
{

    public function index()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
    }

    public function logout(){
      Auth::destroy_identity();
      // Auth::destroy_active_session();
      return Redirect::to('admin/login');
    }

    public function login()
    {
      if(Auth::is_valid()){
        return Redirect::to('admin');
      }
      View::template('login');
      if(Input::hasPost('users')){
        $pwd = Input::post('users.password');
        $correo = Input::post('users.username');
        $auth = new Auth("model", "class: users", "username: $correo", "password: $pwd");
        if ($auth->authenticate()) {
          return Redirect::to("admin");
        }else{
          Flash::error('Usuario o contraseña incorrecta!');
        }
      }
    }

    public function quienes_somos()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('quienes_somos')){
        $this->quienes_somos = (new Quienes_somos(Input::post('quienes_somos')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/quienes_somos/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->quienes_somos->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->quienes_somos->update()) {
          Flash::valid("Quienes somos actualizado");
          return Redirect::to("admin/quienes_somos/");
        }else{
          Flash::error("Error al editar, verifique e intente de nuevo");
          return Redirect::to("admin/quienes_somos/");
        }

      }else{
        $this->quienes_somos = (new Quienes_somos())->find_first();
      }
    }

    public function testimonios()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      $this->testimonios = (new Testimonios())->find();
    }

    public function testimonios_agregar()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('testimonios')){
        $this->testimonios = (new Testimonios(Input::post('testimonios')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/testimonios/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->testimonios->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->testimonios->save()) {
          Flash::valid("Testimonio Registrado");
          return Redirect::to("admin/testimonios/");
        }
      }
    }

    public function testimonios_eliminar($id){
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
      if ((new Testimonios)->delete((int) $id)) {
              Flash::valid('Operación exitosa');
      } else {
              Flash::error('Falló Operación');
      }
      return Redirect::to("admin/testimonios/");
    }

    public function clientes_eliminar($id){
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
      if ((new Clientes)->delete((int) $id)) {
              Flash::valid('Operación exitosa');
      } else {
              Flash::error('Falló Operación');
      }
      return Redirect::to("admin/clientes/");
    }

    public function testimonios_editar($id)
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('testimonios')){
        $this->testimonios = (new Testimonios(Input::post('testimonios')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/testimonios/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->testimonios->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->testimonios->update()) {
          Flash::valid("Testimonio actualizado!");
          return Redirect::to("admin/testimonios/");
        }
      }else{
        $this->testimonios = (new Testimonios())->find_by_id((int)$id);
      }
    }

    public function clientes()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      $this->clientes = (new Clientes())->find();
    }

    public function clientes_agregar()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('clientes')){
        $this->clientes = (new Clientes(Input::post('clientes')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/clientes/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->clientes->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->clientes->save()) {
          Flash::valid("Cliente Registrado");
          return Redirect::to("admin/clientes/");
        }
      }
    }

    public function clientes_editar($id)
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('clientes')){
        $this->clientes = (new Clientes(Input::post('clientes')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/clientes/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->clientes->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->clientes->update()) {
          Flash::valid("Clientes actualizado!");
          return Redirect::to("admin/clientes/");
        }
      }else{
        $this->clientes = (new Clientes())->find_by_id((int)$id);
      }
    }

    public function necesitas_app()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('necesitas_app')){
        $this->necesitas_app = (new Necesitas_app(Input::post('necesitas_app')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/necesitas_app/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->necesitas_app->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->necesitas_app->update()) {
          Flash::valid("Sección necesidad un APP actualizada");
          return Redirect::to("admin/necesitas_app/");
        }else{
          Flash::error("Error al editar, verifique e intente de nuevo");
          return Redirect::to("admin/necesitas_app/");
        }

      }else{
        $this->necesitas_app = (new Necesitas_app())->find_first();
      }
    }

    public function servicios()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      $this->servicios = (new Servicios())->find();
    }

    public function servicios_agregar()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('servicios')){
        $this->servicios = (new Servicios(Input::post('servicios')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/servicios/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->servicios->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->servicios->save()) {
          Flash::valid("Servicio Registrado");
          return Redirect::to("admin/servicios/");
        }
      }
    }

    public function servicios_editar($id)
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('servicios')){
        $this->servicios = (new Servicios(Input::post('servicios')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/servicios/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->servicios->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->servicios->update()) {
          Flash::valid("Servicio actualizado!");
          return Redirect::to("admin/servicios/");
        }
      }else{
        $this->servicios = (new Servicios())->find_by_id((int)$id);
      }
    }

    public function servicios_eliminar($id){
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
      if ((new Servicios)->delete((int) $id)) {
              Flash::valid('Operación exitosa');
      } else {
              Flash::error('Falló Operación');
      }
      return Redirect::to("admin/servicios/");
    }

    public function necesitas() {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('necesitas')){
        $this->necesitas = (new Necesitas(Input::post('necesitas')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/necesitas/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->necesitas->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->necesitas->update()) {
          Flash::valid("Sección necesitas actualizada");
          return Redirect::to("admin/necesitas/");
        }else{
          Flash::error("Error al editar, verifique e intente de nuevo");
          return Redirect::to("admin/necesitas/");
        }

      }else{
        $this->necesitas = (new Necesitas())->find_first();
      }

   }

   public function quieres_web(){
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     if(Input::hasPost('quieres_web')){
       $this->quieres_web = (new Quieres_web(Input::post('quieres_web')));
       if ($this->quieres_web->update()) {
         Flash::valid("Sección quieres un web actualizada");
         return Redirect::to("admin/quieres_web/");
       }else{
         Flash::error("Error al editar, verifique e intente de nuevo");
         return Redirect::to("admin/quieres_web/");
       }

     }else{
       $this->quieres_web = (new Quieres_web())->find_first();
     }
   }

   public function sliders(){
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     $this->sliders = (new Sliders())->find();
   }

   function sliders_agregar(){
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     if(Input::hasPost('sliders')){
       $this->sliders = (new Sliders(Input::post('sliders')));
       if($_FILES){
         $target_path = FILES_PATH.'upload/sliders/';
         $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
         if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
           $this->sliders->imagen = $_FILES['imagen_selected']['name'];
         }
       }
       if ($this->sliders->save()) {
         Flash::valid("Slider Registrado");
         return Redirect::to("admin/sliders/");
       }else{
         Flash::error("Error al registrar el slider");
         return Redirect::to("admin/sliders/");
       }
     }
   }

   public function sliders_editar($id){
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     if(Input::hasPost('sliders')){
       $this->sliders = (new Sliders(Input::post('sliders')));
       if($_FILES){
         $target_path = FILES_PATH.'upload/sliders/';
         $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
         if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
           $this->sliders->imagen = $_FILES['imagen_selected']['name'];
         }
       }
       if ($this->sliders->update()) {
         Flash::valid("Slider actualizado!");
         return Redirect::to("admin/sliders/");
       }
     }else{
       $this->sliders = (new Sliders())->find_by_id((int)$id);
     }
   }

   public function sliders_eliminar($id){
     if(!Auth::is_valid()){
       return Redirect::to('admin/login');
     }
     if ((new Sliders)->delete((int) $id)) {
             Flash::valid('Operación exitosa');
     } else {
             Flash::error('Falló Operación');
     }
     return Redirect::to("admin/sliders/");
   }

   public function contacto()
   {
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     if(Input::hasPost('contacto')){
       $this->contacto = (new Contacto(Input::post('contacto')));
       if ($this->contacto->update()) {
         Flash::valid("Contacto actualizado");
         return Redirect::to("admin/contacto/");
       }else{
         Flash::error("Error al editar, verifique e intente de nuevo");
         return Redirect::to("admin/contacto/");
       }

     }else{
       $this->contacto = (new Contacto())->find_first();
     }
   }
   public function links_footer(){
     $this->links_footer = (new Links_footer())->find();
   }

   public function links_footer_agregar(){
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     if(Input::hasPost('links_footer')){
       $this->links_footer = (new Links_footer(Input::post('links_footer')));
       if ($this->links_footer->save()) {
         Flash::valid("Link Registrado");
         return Redirect::to("admin/links_footer/");
       }else{
         Flash::error("Error al registrar el link");
         return Redirect::to("admin/links_footer/");
       }
     }
   }

   public function links_footer_editar($id){
     if(!Auth::is_valid()){
       return Redirect::to('admin');
     }
     if(Input::hasPost('links_footer')){
       $this->links_footer = (new Links_footer(Input::post('links_footer')));
       if ($this->links_footer->update()) {
         Flash::valid("Link actualizado!");
         return Redirect::to("admin/links_footer/");
       }
     }else{
       $this->links_footer = (new Links_footer())->find_by_id((int)$id);
     }
   }

   public function links_footer_eliminar($id){
     if(!Auth::is_valid()){
       return Redirect::to('admin/login');
     }
     if ((new Finks_footer)->delete((int) $id)) {
             Flash::valid('Operación exitosa');
     } else {
             Flash::error('Falló Operación');
     }
     return Redirect::to("admin/links_footer/");
   }

   public function blogs() {
     $this->blogs = (new Blogs())->find(
       "order: created_at desc"
     );
  }

  public function blogs_agregar() {
    if(!Auth::is_valid()){
      return Redirect::to('admin');
    }
    if(Input::hasPost('blogs')){
      $this->blogs = (new Blogs(Input::post('blogs')));
      if($_FILES){
        $target_path = FILES_PATH.'upload/blogs/';
        $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
        if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
          $this->blogs->imagen = $_FILES['imagen_selected']['name'];
        }
      }
      if ($this->blogs->save()) {
        Flash::valid("Blog Registrado");
        return Redirect::to("admin/blogs/");
      }else{
        Flash::error("Error al registrar el slider");
        return Redirect::to("admin/blogs/");
      }
    }
  }

  public function blogs_editar($id){
    if(!Auth::is_valid()){
      return Redirect::to('admin');
    }
    if(Input::hasPost('blogs')){
      $this->blogs = (new Blogs(Input::post('blogs')));
      if($_FILES){
        $target_path = FILES_PATH.'upload/blogs/';
        $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
        if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
          $this->blogs->imagen = $_FILES['imagen_selected']['name'];
        }
      }
      if ($this->blogs->update()) {
        Flash::valid("blog actualizado!");
        return Redirect::to("admin/blogs/");
      }
    }else{
      $this->blogs = (new Blogs())->find_by_id((int)$id);
    }
  }

  public function blogs_eliminar($id){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Blogs)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to("admin/blogs/");
  }

  public function portafolio(){
    $this->portafolio = (new Portafolio())->find();
  }

  public function portafolio_agregar() {
    if(!Auth::is_valid()){
      return Redirect::to('admin');
    }
    if(Input::hasPost('portafolio')){
      $this->portafolio = (new Portafolio(Input::post('portafolio')));
      if($_FILES){
        $target_path = FILES_PATH.'upload/portafolio/';
        $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
        if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
          $this->portafolio->imagen = $_FILES['imagen_selected']['name'];
        }
      }
      if ($this->portafolio->save()) {
        Flash::valid("Cliente Registrado");
        return Redirect::to("admin/portafolio/");
      }else{
        Flash::error("Error al registrar el cliente");
        return Redirect::to("admin/portafolio/");
      }
    }
  }

  public function portafolio_editar($id){
    if(!Auth::is_valid()){
      return Redirect::to('admin');
    }
    if(Input::hasPost('portafolio')){
      $this->portafolio = (new Portafolio(Input::post('portafolio')));
      if($_FILES){
        $target_path = FILES_PATH.'upload/portafolio/';
        $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
        if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
          $this->portafolio->imagen = $_FILES['imagen_selected']['name'];
        }
      }
      if ($this->portafolio->update()) {
        Flash::valid("Cliente actualizado!");
        return Redirect::to("admin/portafolio/");
      }
    }else{
      $this->portafolio = (new Portafolio())->find_by_id((int)$id);
    }
  }

  public function portafolio_eliminar($id){
    if(!Auth::is_valid()){
      return Redirect::to('admin/login');
    }
    if ((new Portafolio)->delete((int) $id)) {
            Flash::valid('Operación exitosa');
    } else {
            Flash::error('Falló Operación');
    }
    return Redirect::to("admin/portafolio/");
  }
}
